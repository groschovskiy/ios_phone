//
//  CellularDialling.m
//  Telephone
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "CellularDialling.h"
#import <Parse/Parse.h>

@interface CellularDialling () {
    double cost;
    double _ticker;
    NSInteger *seconds;
    NSInteger *minutes;
    NSInteger *miniSeconds;
}

@end

@implementation CellularDialling

- (void)viewDidLoad {
    cost = [self.declarationPrice floatValue];
    self.callingNumber.text = self.declarationNumber;
    [super viewDidLoad];

    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sensorDetectSomthing:)
                                                 name:@"UIDeviceProximityStateDidChangeNotification" object:nil];

    NSTimer *carrierCostTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];

    NSTimer *carrierCostInit = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(paymentAccountRelay:) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Read information from the proximity sensor

- (void)sensorDetectSomthing:(NSNotificationCenter *)notification {
    if ([[UIDevice currentDevice] proximityState] == YES)
        NSLog(@"Debug: iPhone is close to some Object");
    else
        NSLog(@"Debug: There is nothing near to iPhone");
}

- (void)timerTick:(NSTimer *)timer {
    _ticker += 5;
    double seconds = fmod(_ticker, 60.0);
    double minutes = fmod(trunc(_ticker / 60.0), 60.0);
    double hours = trunc(_ticker / 3600.0);
    self.callingTime.text = [NSString stringWithFormat:@"%02.0f:%02.0f", hours, minutes, seconds];
}

- (void)paymentAccountRelay:(NSTimer *)payPerMinute {
    PFUser *carrierServiceID = [PFUser currentUser][@"phoneNumber"];
    PFQuery *terminal = [PFQuery queryWithClassName:@"GCHQPhone"];
    [terminal getObjectInBackgroundWithId:carrierServiceID block:^(PFObject *paymentMonitor, NSError *error) {
        NSString *carrierSpecialPrice = paymentMonitor[@"specialPrice"];
        NSString *carrierCreditValue = paymentMonitor[@"currentCredit"];
        float customerCreditValue = [carrierCreditValue floatValue];
        float callingPrice  = [carrierSpecialPrice floatValue];
        float transactionCreditValue = customerCreditValue - callingPrice;
        NSString *transactionEvent = [NSString stringWithFormat:@"%f", transactionCreditValue];
        
        paymentMonitor[@"currentCredit"] = transactionEvent;
        [paymentMonitor saveInBackground];
        NSLog(@"%@", paymentMonitor);
    }];
}

#pragma mark - Additional Feature Event

- (IBAction)rejectPhoneCall:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)functionMuteMicrophone:(id)sender {
    
}

- (IBAction)functionKeypadView:(id)sender {
    
}

- (IBAction)functionSpeakerEnable:(id)sender {
    
}

- (IBAction)functionAddingContact:(id)sender {
    
}

- (IBAction)functionFaceTime:(id)sender {
    
}

- (IBAction)functionContactView:(id)sender {
    
}

@end
