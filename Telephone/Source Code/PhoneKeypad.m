//
//  PhoneKeypad.m
//  Telephone
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "PhoneKeypad.h"
#import "CellularDialling.h"
#import <Parse/Parse.h>

@interface PhoneKeypad () {
    NSString *stack;
}

@end

@implementation PhoneKeypad

- (void)viewDidLoad {
    stack = @"";
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Dialling Pad Buttons Event

- (IBAction)clenStack:(id)sender {
    stack = @"";
    self.phoneNumber.text = @"";
}

- (IBAction)digitOne:(id)sender {
    [self addnumber:1];
}

- (IBAction)digitTwo:(id)sender {
    [self addnumber:2];
}

- (IBAction)digitThree:(id)sender {
    [self addnumber:3];
}

- (IBAction)digitFour:(id)sender {
    [self addnumber:4];
}

- (IBAction)digitFive:(id)sender {
    [self addnumber:5];
}

- (IBAction)digitSix:(id)sender {
    [self addnumber:6];
}

- (IBAction)digitSeven:(id)sender {
    [self addnumber:7];
}

- (IBAction)digitEight:(id)sender {
    [self addnumber:8];
}

- (IBAction)digitNine:(id)sender {
    [self addnumber:9];
}

- (IBAction)digitZero:(id)sender {
    [self addnumber:0];
}

- (IBAction)digitShark:(id)sender {
    NSString *asterisk = @"*";
    stack = [NSString stringWithFormat:@"%@%@", stack, asterisk];
    [self.phoneNumber setText:stack];
}

- (IBAction)digitHash:(id)sender {
    NSString *asterisk = @"#";
    stack = [NSString stringWithFormat:@"%@%@", stack, asterisk];
    [self.phoneNumber setText:stack];
}

-(void)addnumber:(char)number{
    stack = [NSString stringWithFormat:@"%1$@%2$d", stack, number];
    [self.phoneNumber setText:stack];
}

#pragma mark - Phone Calling Event

- (IBAction)executeCallingEvent:(id)sender {
    if ([stack isEqualToString:@"*450#"] == YES) {
        [self accountCreditChecking];
    } else if ([stack isEqualToString:@"*999#"] == YES) {
        [self accountServiceControl];
    } else if ([stack isEqualToString:@"*710#"] == YES)  {
        [self accountPaymentControl];
    } else if ([stack isEqualToString:@"*888#"] == YES)  {
        [self accountLTEControl];
    } else if ([stack isEqualToString:@"*140*7#"] == YES)  {
        [self accountDataInRoadmin];
    } else if ([stack isEqualToString:@"440710200500"] == YES) {
        NSLog(@"Credit");
    } else {
        NSString *numberAlarm = self.phoneNumber.text;
        CellularDialling *callingEvent = [[CellularDialling alloc] initWithNibName:@"CellularDialling" bundle:nil];
        callingEvent.declarationNumber = numberAlarm;
        callingEvent.declarationPrice = @"2";
        [self presentViewController:callingEvent animated:YES completion:nil];
    }
}

#pragma mark - Carrier Programming Lound

- (void)accountCreditChecking {
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query getObjectInBackgroundWithId:currentUser.objectId block:^(PFObject *paymentEvent, NSError *error) {
        NSString *phoneProviderNumber = paymentEvent[@"phoneNumber"];
        PFQuery *query = [PFQuery queryWithClassName:@"GCHQPhone"];
        [query getObjectInBackgroundWithId:phoneProviderNumber block:^(PFObject *paymentEvent, NSError *error) {
            NSString *currentCredit = paymentEvent[@"currentCredit"];
            float accountCredit = [currentCredit floatValue];
            NSString *USSDCredit = [NSString stringWithFormat:@"Your current credit - %.2f. Account valid until to 01 Jan 2017.", accountCredit];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"EE Service" message:(@"", USSDCredit) delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            [alert show];
            
            NSLog(@"%@", paymentEvent);
        }];
        NSLog(@"%@", paymentEvent);
    }];
}

- (void)accountLTEControl {
    PFUser *carrierServiceID = [PFUser currentUser][@"phoneNumber"];
    PFQuery *terminal = [PFQuery queryWithClassName:@"GCHQPhone"];
    [terminal getObjectInBackgroundWithId:carrierServiceID block:^(PFObject *paymentMonitor, NSError *error) {
        NSString *carrierCreditValue = paymentMonitor[@"currentCredit"];
        float customerCreditValue = [carrierCreditValue floatValue];
        float transactionCreditValue = customerCreditValue - 2;
        NSString *transactionEvent = [NSString stringWithFormat:@"%f", transactionCreditValue];
        
        paymentMonitor[@"currentCredit"] = transactionEvent;
        [paymentMonitor saveInBackground];
        NSLog(@"%@", paymentMonitor);
    }];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lloyds Banking" message:@"You account success top up! Please check accout credit after little time. Account ballance may update after little delay. Thank you!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    [alert show];
}

- (void)accountDataInRoadmin {
    PFUser *carrierServiceID = [PFUser currentUser][@"phoneNumber"];
    PFQuery *terminal = [PFQuery queryWithClassName:@"GCHQPhone"];
    [terminal getObjectInBackgroundWithId:carrierServiceID block:^(PFObject *paymentMonitor, NSError *error) {
        NSString *carrierCreditValue = paymentMonitor[@"currentCredit"];
        float customerCreditValue = [carrierCreditValue floatValue];
        float transactionCreditValue = customerCreditValue - 3;
        NSString *transactionEvent = [NSString stringWithFormat:@"%f", transactionCreditValue];
        
        paymentMonitor[@"currentCredit"] = transactionEvent;
        [paymentMonitor saveInBackground];
        NSLog(@"%@", paymentMonitor);
    }];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"EE Service" message:@"The service ""50MB roaming"" successfully connected to your phone number. Please uselessness of service disconnect it from your phone number." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    [alert show];
}

- (void)accountServiceControl {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"EE Service" message:@"Everything Everywhere Lounge\nCurrent Data: 74 GB\nCurrent Minutes: 290\nCurrent Messages: 2500" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    [alert show];
}

- (void)accountPaymentControl {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lloyds Banking" message:@"You account success top up! Please check accout credit after little time. Account ballance may update after little delay. Thank you!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    [alert show];
}

@end
