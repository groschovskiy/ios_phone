//
//  PhoneHistoryCell.h
//  Telephone
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneHistoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *phoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *phoneCountry;
@property (weak, nonatomic) IBOutlet UIImageView *phoneContactView;

@end
