//
//  PhoneHistory.m
//  Telephone
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "PhoneHistory.h"
#import "PhoneHistoryCell.h"
#import "CellularDialling.h"

@interface PhoneHistory () {
    NSArray *phoneNumber;
    NSArray *phoneCountry;
    NSArray *phoneCallDate;
}

@end

@implementation PhoneHistory

- (void)viewDidLoad {
    
    phoneNumber = [[NSArray alloc] initWithObjects:@"+1 (228) 389-2881", @"+1 (800) 640-0000", @"+44 (0742) 289-1993", @"+44 (0742) 200-2910", @"+49 (29) 200-9102", @"Natalia Korobko", @"Dawid Ostrowsky", @"Emmy Betts", nil];
    
    phoneCountry = [[NSArray alloc] initWithObjects:@"United States, Verizon", @"United States, Toll-Free", @"United Kingdom, T-Mobile", @"United Kingdom, Vodafone", @"German, O2", @"Poland, T-Mobile", @"Poland, T-Mobile", @"United Kingdom, O2", nil];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [phoneNumber count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PhoneHistoryCell *phoneHistory = [tableView dequeueReusableCellWithIdentifier:@"phoneNumberCell"];
    if (!phoneHistory) {
        [tableView registerNib:[UINib nibWithNibName:@"PhoneHistoryCell" bundle:nil] forCellReuseIdentifier:@"phoneNumberCell"];
        phoneHistory = [tableView dequeueReusableCellWithIdentifier:@"phoneNumberCell"];
    }
    
    return phoneHistory;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(PhoneHistoryCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.phoneNumber.text = [phoneNumber objectAtIndex:indexPath.row];
    cell.phoneCountry.text = [phoneCountry objectAtIndex:indexPath.row];
}

- (void)tableView:(nonnull UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    NSString *callerNumber = [phoneNumber objectAtIndex:indexPath.row];
    NSString *callerCountry = [phoneCountry objectAtIndex:indexPath.row];
    
    CellularDialling *cellularInfo = [[CellularDialling alloc] initWithNibName:@"CellularDialling" bundle:nil];
    cellularInfo.declarationNumber = callerNumber;
    [self presentViewController:cellularInfo animated:YES completion:nil];
}

@end
