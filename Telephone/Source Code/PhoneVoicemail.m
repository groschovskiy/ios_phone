//
//  PhoneVoicemail.m
//  Telephone
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "PhoneVoicemail.h"
#import "CarrierPortal.h"
#import <Parse/Parse.h>

@interface PhoneVoicemail ()

@end

@implementation PhoneVoicemail

- (void)viewDidLoad {
    [super viewDidLoad];
    [self authWithCredentials];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Auth with Carrier Service

- (IBAction)showCarrierSIMService:(id)sender {
    CarrierPortal *carrierService = [[CarrierPortal alloc] initWithNibName:@"CarrierPortal" bundle:nil];
    [self presentViewController:carrierService animated:true completion:nil];
    NSLog(@"Carrier: SIM Service Menu initialized!");
}

#pragma mark - Internet Provider Credentials

- (void)authWithCredentials {
    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"EE Service" message:@"Enter EE or T-Mobile personal contract number. If you can't auth, please contact with service center." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
    alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    [alert addButtonWithTitle:@"Login"];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1)
    {
        UITextField *username = [alertView textFieldAtIndex:0];
        NSLog(@"Username: %@", username.text);
        
        UITextField *password = [alertView textFieldAtIndex:1];
        NSLog(@"Password: %@", password.text);
        
        [PFUser logInWithUsernameInBackground:username.text password:password.text
                                        block:^(PFUser *user, NSError *error) {
                                            if (user) {
                                                NSLog(@"Success!");
                                                [self dismissViewControllerAnimated:true completion:nil];
                                            } else {
                                                NSLog(@"Failed!");
                                                [self dismissViewControllerAnimated:true completion:nil];
                                            }
                                        }];
    }
}

@end
