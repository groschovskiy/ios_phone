//
//  PhoneContacts.h
//  Telephone
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneContacts : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    NSMutableArray *menuArray;
    NSString *firstName;
    NSString *lastName;
    UIButton *addcontact;
    IBOutlet UITableView *tableView;
}

@property (nonatomic, retain) NSMutableArray *menuArray;
@property (nonatomic, strong) UIButton *addcontact;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;

-(void)showPeoplePickerController;

@end
