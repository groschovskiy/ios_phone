//
//  CellularDialling.h
//  Telephone
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellularDialling : UIViewController

@property (weak, nonatomic) NSString *declarationPrice;
@property (weak, nonatomic) NSString *declarationNumber;
@property (weak, nonatomic) IBOutlet UILabel *callingNumber;
@property (weak, nonatomic) IBOutlet UILabel *callingTime;

@end
