//
//  PhoneContacts.m
//  Telephone
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "PhoneContacts.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface PhoneContacts () <ABPeoplePickerNavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate> {
    NSArray *addressBook;
    NSArray *phoneNumber;
    NSArray *phoneName;
}

@property (nonatomic, strong) NSMutableArray *peopleSelected;

@end

@implementation PhoneContacts

- (void)viewDidLoad {
    [super viewDidLoad];
    
    menuArray = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Address Book UI Controller

- (BOOL)peoplePickerNavigationController:
    (ABPeoplePickerNavigationController *)peoplePicker
    shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    
    firstName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    [menuArray addObject:firstName];
    lastName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    [menuArray addObject:lastName];
    
    [self dismissModalViewControllerAnimated:YES];
    return NO;
    [tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSString *cellValue = [menuArray objectAtIndex:indexPath.row];
    cell.textLabel.text = cellValue;
    NSLog(@"%@", cellValue	);
    
    return cell;
}

- (IBAction)addContact:(id)sender {
    ABPeoplePickerNavigationController *peoplePicker=[[ABPeoplePickerNavigationController alloc] init];
    [peoplePicker setPeoplePickerDelegate:self];
    [self presentModalViewController:peoplePicker animated:YES];
}

@end
