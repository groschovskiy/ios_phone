//
//  CarrierPortal.m
//  Telephone
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "CarrierPortal.h"
#import <Parse/Parse.h>

@interface CarrierPortal ()

@end

@implementation CarrierPortal

- (void)viewDidLoad {
    [self authWithCarrierInformation];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Authorize with

- (void)authWithCarrierInformation {
    PFUser *carrierServiceID = [PFUser currentUser][@"phoneNumber"];
    PFQuery *query = [PFQuery queryWithClassName:@"GCHQPhone"];
    [query getObjectInBackgroundWithId:carrierServiceID block:^(PFObject *accountInformation, NSError *error) {
        self.fullName.text = accountInformation[@"contractPerson"];
        self.phoneNumber.text = accountInformation[@"phoneNumber"];
        self.currentCredit.text = accountInformation[@"currentCredit"];
        NSLog(@"%@", accountInformation);
    }];
}

#pragma mark - Pay with bank account

- (IBAction)refillFivePounds:(id)sender {
    creditValue = 5;
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query getObjectInBackgroundWithId:currentUser.objectId block:^(PFObject *paymentEvent, NSError *error) {
        NSString *phoneProviderNumber = paymentEvent[@"phoneNumber"];
        PFQuery *query = [PFQuery queryWithClassName:@"GCHQPhone"];
        [query getObjectInBackgroundWithId:phoneProviderNumber block:^(PFObject *paymentEvent, NSError *error) {
            NSString *currentCredit = paymentEvent[@"currentCredit"];
            float receivedCredit = [currentCredit floatValue];
            float totalCredit = receivedCredit + creditValue;
            NSString *newBallance = [NSString stringWithFormat:@"%f", totalCredit];
            
            PFQuery *query = [PFQuery queryWithClassName:@"GCHQPhone"];
            [query getObjectInBackgroundWithId:phoneProviderNumber
                                         block:^(PFObject *phoneStatistics, NSError *error) {
                                             phoneStatistics[@"currentCredit"] = newBallance;
                                             [phoneStatistics saveInBackground];
                                         }];
            
            NSLog(@"%@", paymentEvent);
        }];
        NSLog(@"%@", paymentEvent);
    }];
    
    [self bankPaymentRequest];
}

- (IBAction)refillTenPounds:(id)sender {
    creditValue = 10;
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query getObjectInBackgroundWithId:currentUser.objectId block:^(PFObject *paymentEvent, NSError *error) {
        NSString *phoneProviderNumber = paymentEvent[@"phoneNumber"];
        PFQuery *query = [PFQuery queryWithClassName:@"GCHQPhone"];
        [query getObjectInBackgroundWithId:phoneProviderNumber block:^(PFObject *paymentEvent, NSError *error) {
            NSString *currentCredit = paymentEvent[@"currentCredit"];
            float receivedCredit = [currentCredit floatValue];
            float totalCredit = receivedCredit + creditValue;
            NSString *newBallance = [NSString stringWithFormat:@"%f", totalCredit];
            
            PFQuery *query = [PFQuery queryWithClassName:@"GCHQPhone"];
            [query getObjectInBackgroundWithId:phoneProviderNumber
                                         block:^(PFObject *phoneStatistics, NSError *error) {
                                             phoneStatistics[@"currentCredit"] = newBallance;
                                             [phoneStatistics saveInBackground];
                                         }];
            
            NSLog(@"%@", paymentEvent);
        }];
        NSLog(@"%@", paymentEvent);
    }];
    
    [self bankPaymentRequest];
}

- (IBAction)refillTwentyFivePounds:(id)sender {
    creditValue = 25;
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query getObjectInBackgroundWithId:currentUser.objectId block:^(PFObject *paymentEvent, NSError *error) {
        NSString *phoneProviderNumber = paymentEvent[@"phoneNumber"];
        PFQuery *query = [PFQuery queryWithClassName:@"GCHQPhone"];
        [query getObjectInBackgroundWithId:phoneProviderNumber block:^(PFObject *paymentEvent, NSError *error) {
            NSString *currentCredit = paymentEvent[@"currentCredit"];
            float receivedCredit = [currentCredit floatValue];
            float totalCredit = receivedCredit + creditValue;
            NSString *newBallance = [NSString stringWithFormat:@"%f", totalCredit];
            
            PFQuery *query = [PFQuery queryWithClassName:@"GCHQPhone"];
            [query getObjectInBackgroundWithId:phoneProviderNumber
                                         block:^(PFObject *phoneStatistics, NSError *error) {
                                             phoneStatistics[@"currentCredit"] = newBallance;
                                             [phoneStatistics saveInBackground];
                                         }];
            
            NSLog(@"%@", paymentEvent);
        }];
        NSLog(@"%@", paymentEvent);
    }];
    
    [self bankPaymentRequest];
}

- (IBAction)refillFiftyPounds:(id)sender {
    creditValue = 50;
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query getObjectInBackgroundWithId:currentUser.objectId block:^(PFObject *paymentEvent, NSError *error) {
        NSString *phoneProviderNumber = paymentEvent[@"phoneNumber"];
        PFQuery *query = [PFQuery queryWithClassName:@"GCHQPhone"];
        [query getObjectInBackgroundWithId:phoneProviderNumber block:^(PFObject *paymentEvent, NSError *error) {
            NSString *currentCredit = paymentEvent[@"currentCredit"];
            float receivedCredit = [currentCredit floatValue];
            float totalCredit = receivedCredit + creditValue;
            NSString *newBallance = [NSString stringWithFormat:@"%f", totalCredit];
            
            PFQuery *query = [PFQuery queryWithClassName:@"GCHQPhone"];
            [query getObjectInBackgroundWithId:phoneProviderNumber
                                         block:^(PFObject *phoneStatistics, NSError *error) {
                                             phoneStatistics[@"currentCredit"] = newBallance;
                                             [phoneStatistics saveInBackground];
                                         }];
            
            NSLog(@"%@", paymentEvent);
        }];
        NSLog(@"%@", paymentEvent);
    }];
    
    [self bankPaymentRequest];
}

#pragma mark - Bank Account Number Request

- (void)bankPaymentRequest {
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query getObjectInBackgroundWithId:currentUser.objectId block:^(PFObject *paymentEvent, NSError *error) {
        NSString *bankProviderNumber = paymentEvent[@"bankingNumber"];
        PFQuery *query = [PFQuery queryWithClassName:@"LLOYBanking"];
        [query getObjectInBackgroundWithId:bankProviderNumber block:^(PFObject *paymentEvent, NSError *error) {
            NSString *currentCredit = paymentEvent[@"currentCredit"];
            float floatCredit = [currentCredit floatValue];
            float convertCredit = floatCredit - creditValue;
            NSString *newBallance = [NSString stringWithFormat:@"%f", convertCredit];
            
            PFQuery *query = [PFQuery queryWithClassName:@"LLOYBanking"];
            [query getObjectInBackgroundWithId:bankProviderNumber
                                         block:^(PFObject *bankTransaction, NSError *error) {
                                             bankTransaction[@"currentCredit"] = newBallance;
                                             [bankTransaction saveInBackground];
                                         }];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"EE Service" message:@"Dear Subscriber! Your account balance has been successfully refilled. A few seconds later the payment credited to your personal account. Thank you for using the services of T-Mobile and EE UK." delegate:nil cancelButtonTitle:@"Confirm" otherButtonTitles:nil];
            [alert show];
            [self authWithCarrierInformation];
            
            NSLog(@"%@", paymentEvent);
        }];
        NSLog(@"%@", paymentEvent);
    }];
}

@end
