//
//  PhoneKeypad.h
//  Telephone
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneKeypad : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;

@end
